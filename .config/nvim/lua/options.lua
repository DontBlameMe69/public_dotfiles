require "nvchad.options"

vim.o.tabstop = 4 -- A TAB character looks like 4 spaces
vim.o.expandtab = true -- Pressing the TAB key will insert spaces instead of a TAB character
vim.o.softtabstop = 4 -- Number of spaces inserted instead of a TAB character
vim.o.shiftwidth = 4 -- Number of spaces inserted when indenting
vim.o.inccommand = "split" -- Incremental live completion
vim.o.backspace = "indent,eol,start" -- Backspace key behaves like backspace in most applications

-- Disable bottom line (status bar)
vim.o.cmdheight = 0
vim.o.laststatus = 0

-- Better breakpoints
vim.fn.sign_define("DapBreakpoint", { text = "•", texthl = "red", linehl = "DapBreakpoint", numhl = "DapBreakpoint" })
vim.fn.sign_define(
  "DapBreakpointCondition",
  { text = "•", texthl = "red", linehl = "DapBreakpoint", numhl = "DapBreakpoint" }
)
vim.fn.sign_define(
  "DapBreakpointRejected",
  { text = "•", texthl = "orange", linehl = "DapBreakpoint", numhl = "DapBreakpoint" }
)
vim.fn.sign_define("DapStopped", { text = "•", texthl = "green", linehl = "DapBreakpoint", numhl = "DapBreakpoint" })
vim.fn.sign_define(
  "DapLogPoint",
  { text = "•", texthl = "yellow", linehl = "DapBreakpoint", numhl = "DapBreakpoint" }
)

-- Scrolloff
vim.o.scrolloff = 8

-- Smart indenting
vim.o.smartindent = true

-- Wrapping
vim.o.wrap = true

-- Turn off vim undo due to undotree
vim.o.swapfile = false
vim.o.backup = false
vim.o.undodir = os.getenv "HOME" .. "/.config/nvim/undodir"
vim.o.undofile = true

-- Faster update time
vim.o.updatetime = 50
vim.o.signcolumn = "yes"

-- Line numbers
vim.o.number = true

-- Relative line numbers
vim.o.relativenumber = true

-- Disable mouse
vim.o.mouse = ""

-- Enable list
vim.o.list = true

-- Enable file backups
vim.o.undofile = true
