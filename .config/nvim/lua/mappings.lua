require "nvchad.mappings"

local map = vim.keymap.set
local unmap = vim.keymap.del

-- Unbind
unmap("n", "<Leader>e")
unmap("n", "<Leader>n")
unmap("n", "<Leader>/")
unmap("n", "<Leader>x")
unmap("n", "<Leader>wk")
unmap("n", "<Leader>wK")
unmap("n", "<Leader>rn")
unmap("n", "<Leader>ma")
unmap("n", "<Leader>pt")
unmap("n", "<Leader>cm")
unmap("n", "<Leader>gt")
unmap("n", "<C-s>")
unmap("n", "<C-k>")
unmap("n", "<C-j>")
unmap("n", "<C-l>")
unmap("n", "<C-c>")
unmap("n", "<C-h>")
unmap("n", "<C-w><C-d>")
unmap("n", "<C-w>d")
unmap("n", "<M-i>")
unmap("n", "<M-h>")
unmap("n", "<M-v>")
unmap("n", "[d")
unmap("n", "]d")
unmap("v", "<Leader>/")
unmap("n", "<Leader>ch")
unmap("n", "<C-n>")

-- Unamp annoying help page
map("n", "<F1>", "<Nop>")

-- Colemak DH Remaps
map("n", "n", "h", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "n", "h", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("o", "n", "h", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "e", "gj", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "e", "j", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("o", "e", "j", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "E", "<C-D>zz", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "E", "<C-D>zz", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("o", "E", "<C-D>zz", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "i", "gk", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "i", "k", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("o", "i", "g", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "I", "<C-U>zz", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "I", "<C-U>zz", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("o", "I", "<C-U>zz", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "o", "l", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "o", "l", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("o", "o", "l", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "l", "b", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "l", "b", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("o", "l", "b", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "L", "B", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "L", "B", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("o", "L", "B", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "u", "e", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "u", "e", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("o", "u", "e", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "U", "E", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "U", "E", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("o", "U", "E", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "t", "a", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "T", "A", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "a", "i", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "A", "I", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "k", "u", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "k", "u", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "<Leader>k", "<C-r>", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "<Leader>k", "<C-r>", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("o", "h", "nzzzv", { desc = "Mapping Colemak DH", noremap = true })
map("o", "H", "Nzzzv", { desc = "Mapping Colemak DH", noremap = true })
map("o", "r", "i", { desc = "Mapping Colemak DH", noremap = true })
map("n", "gX", "X", { desc = "Mapping Colemak DH", noremap = true })
map("x", "gX", "X", { desc = "Mapping Colemak DH", noremap = true })
map("n", "gK", "K", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "gK", "K", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "gL", "L", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "gL", "L", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "<C-W>n", "<C-W>h", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "<C-W>n", "<C-W>h", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "<C-W>e", "<C-W>j", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "<C-W>e", "<C-W>j", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "<C-W>i", "<C-W>k", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "<C-W>i", "<C-W>k", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "<C-W>o", "<C-W>l", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "<C-W>o", "<C-W>l", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "z", "o", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "Z", "O", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "z", "o", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "Z", "O", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "h", "n", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("n", "H", "N", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "t", "I", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("v", "t", "I", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("x", "a", "I", { desc = "Mapping Colemak DH", noremap = true, silent = true })
map("v", "a", "I", { desc = "Mapping Colemak DH", noremap = true, silent = true })

-- Don't copy x to clipboard
map("n", "x", '"_x')
map("v", "x", '"_x')
map("o", "x", '"_x')

-- Move when highlighted
map("v", "E", ":m '>+1<CR>gv=gv", { desc = "Move line down", noremap = true })
map("v", "I", ":m '<-2<CR>gv=gv", { desc = "Move line up", noremap = true })

-- Oil
map("n", "<Leader>e", ":Oil --float<CR>", { desc = "Open Oil", noremap = true })

-- Rename
map("n", "<Leader>r", ":IncRename ", { desc = "Rename", noremap = true })

-- Telescope TODO
map("n", "<Leader>ft", ":TodoTelescope<CR>", { desc = "Find TODOs", noremap = true })

-- Git
map("n", "<Leader>gb", ":Gitsigns blame_line<CR>", { desc = "Gitsigns Blame Line", noremap = true })
map("n", "<Leader>gp", ":Gitsigns preview_hunk<CR>", { desc = "Gitsigns Preview Hunk", noremap = true })

-- Fast Exit
map(
  "n",
  "<Leader>q",
  ":lua if #vim.fn.getbufinfo({buflisted = 1}) > 1 then vim.cmd('w | bdelete') else vim.cmd('wq') end<CR>",
  { desc = "Fast Exit with save", noremap = true, silent = true }
)
map(
  "n",
  "<Leader>Q",
  ":lua if #vim.fn.getbufinfo({buflisted = 1}) > 1 then vim.cmd('bdelete!') else vim.cmd('q!') end<CR>",
  { desc = "Fast Exit without save", noremap = true, silent = true }
)

-- Not clear after one paste
map("v", "p", '"_dP"', { desc = "Multi paste", noremap = true })

-- Replace fast
map("n", "<Leader>s", '"hy:%s//gi<left><left><left>', { desc = "Search and replace", noremap = true })

-- Indent using Tab
map("v", "<Tab>", ">gv", { desc = "Indent", noremap = true, silent = true })
map("n", "<Tab>", ">>", { desc = "Indent", noremap = true, silent = true })

-- Unindent using Shift+Tab
map("v", "<S-Tab>", "<gv", { desc = "Remove indention", noremap = true })
map("n", "<S-Tab>", "<<", { desc = "Remove indention", noremap = true })
map("i", "<S-Tab>", "<C-d>", { desc = "Remove indention", noremap = true })

-- Split
map("n", "<Leader>h", ":vsplit<CR>", { desc = "Split horizontal", noremap = true })
map("n", "<Leader>v", ":split<CR>", { desc = "Split vertical", noremap = true })
map("n", "<ac-n>", "<C-w>>", { desc = "Changing size of splits", noremap = true })
map("n", "<ac-e>", "<C-w>-", { desc = "Changing size of splits", noremap = true })
map("n", "<ac-i>", "<C-w>+", { desc = "Changing size of splits", noremap = true })
map("n", "<ac-o>", "<C-w><", { desc = "Changing size of splits", noremap = true })

-- Tab
map("n", "<Leader>t", ":tabnew<CR>", { desc = "Create tab", noremap = true })
map("n", "<Leader>tp", ":tabprevious<CR>", { desc = "Previous tab", noremap = true })
map("n", "<Leader>tn", ":tabnext<CR>", { desc = "Next tab", noremap = true })

-- Buffers
map("n", "<Leader>bp", ":bp<CR>", { desc = "Previous buffer", noremap = true })
map("n", "<Leader>bn", ":bn<CR>", { desc = "Next buffer", noremap = true })

-- Quick shell script
map(
  "n",
  "<Leader>ms",
  "i#!/usr/bin/env sh<Esc>:w<CR>:e %<CR>:!chmod u+x %<CR>",
  { desc = "Automatically convert to a shell script", noremap = true }
)
map(
  "n",
  "<Leader>mb",
  "i#!/usr/bin/env bash<Esc>:w<CR>:e %<CR>:!chmod u+x %<CR>",
  { desc = "Automatically convert to a shell script", noremap = true }
)
map(
  "n",
  "<Leader>mz",
  "i#!/usr/bin/env zsh<Esc>:w<CR>:e %<CR>:!chmod u+x %<CR>",
  { desc = "Automatically convert to a shell script", noremap = true }
)

-- Floating terminal
map({ "n", "t" }, "<C-t>", function()
  require("nvchad.term").toggle { pos = "float", id = "floatTerm", float_opts = {
    border = "rounded",
  } }
end, { desc = "Toggle Floating Terminal" })

-- Rust
map("n", "<Leader>rr", ":RustLsp runnables<CR>", { desc = "Run a specific rust runnable", noremap = true })
map("n", "<Leader>rh", ":RustLsp hover actions<CR>", { desc = "Show hover information", noremap = true })
map("n", "<Leader>rd", ":RustLsp debuggables<CR>", { desc = "Debug the current project", noremap = true })
map("n", "<Leader>rcu", ":Crates upgrade_all_crates<CR>", { desc = "Upgrade all crates", noremap = true })

-- Debugging
map("n", "<Leader>db", ":DapToggleBreakpoint<CR>", { desc = "Toggle breakpoint", noremap = true })
map("n", "<Leader>ds", ":DapStepOver<CR>", { desc = "Dap Step Over", noremap = true })
map("n", "<Leader>di", ":DapStepInto<CR>", { desc = "Dap Step Into", noremap = true })
map("n", "<Leader>do", ":DapStepOut<CR>", { desc = "Dap Step Out", noremap = true })
map("n", "<Leader>dt", ":DapTerminate<CR>", { desc = "Dap Terminate", noremap = true })
map("n", "<Leader>dv", function()
  local widgets = require "dap.ui.widgets"
  local sidebar = widgets.sidebar(widgets.scopes)

  sidebar.open()
end, { desc = "Dap sidebar UI", noremap = true })

-- Goto
map("n", "<Leader>gd", ":lua vim.lsp.buf.definition()<CR>", { desc = "Goto definition", noremap = true })
map("n", "<Leader>gD", ":lua vim.lsp.buf.declaration()<CR>", { desc = "Goto declaration", noremap = true })
map("n", "<Leader>gi", ":Telescope lsp_implementations<CR>", { desc = "Goto implementation", noremap = true })
map("n", "<Leader>gr", ":Telescope lsp_references<CR>", { desc = "Goto references", noremap = true })
map("n", "<Leader>gh", ":lua vim.lsp.buf.hover()<CR>", { desc = "Show hover information", noremap = true })
map("n", "<Leader>ga", ":lua vim.lsp.buf.code_action()<CR>", { desc = "Show code actions", noremap = true })
