#!/bin/zsh

# Use nvim as manpager
export MANPAGER='nvim +Man!'

# Custom / Longer history
HISTSIZE=10000000
SAVEHIST=10000000
HISTFILE="$XDG_CONFIG_HOME/zsh/.zshhistory"
HISTDUP=erase
setopt appendhistory sharehistory hist_ignore_space hist_ignore_all_dups hist_save_no_dups hist_ignore_dups hist_find_no_dups

# case-insensitive (uppercase from lowercase) completion
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
# enable directory completion colors
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)

# Vi mode
bindkey -v
export KEYTIMEOUT=1

# Vi mode Colemak bindings
bindkey -M vicmd 'n' vi-backward-char
bindkey -M vicmd 'e' vi-down-line-or-history
bindkey -M vicmd 'i' vi-up-line-or-history
bindkey -M vicmd 'o' vi-forward-char
bindkey -M vicmd 'a' vi-insert
bindkey -M vicmd 'r' vi-insert
bindkey -M vicmd 's' vi-insert
bindkey -M vicmd 't' vi-insert

# Use vim keys in tab complete menu:
bindkey -M menuselect 'n' vi-backward-char
bindkey -M menuselect 'e' vi-down-line-or-history
bindkey -M menuselect 'i' vi-up-line-or-history
bindkey -M menuselect 'o' vi-forward-char
bindkey -v '^?' backward-delete-char
