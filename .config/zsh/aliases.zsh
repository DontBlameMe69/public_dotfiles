#!/bin/zsh

# Sudoedit
alias se="sudoedit"

# Lazygit
alias lg="lazygit"

# Disk Usage
alias du="du -sh *"
alias dua="ncdu"

# Extracting
alias extract="aunpack"
# Better fzf
alias sk="fzf --preview='$SCRIPTS/fzf_preview {}' --inline-info"

# Better duf
alias duf="duf --hide-fs tmpfs"

# Manage Dotfiles
alias dotf="/usr/bin/git --git-dir=$XDG_CONFIG_HOME/private_git_dotfiles/ --work-tree=$HOME"

# Manage Public Dotfiles
alias pdotf="/usr/bin/git --git-dir=$XDG_CONFIG_HOME/public_git_dotfiles/ --work-tree=$HOME"

# Better LS
alias ls="lsd -lA --group-dirs=first"

# Vim Bindings
alias v="nvim"
alias q="exit"
alias :q="exit"

# Pretty clear
alias clear="clear && macchina"
alias c="clear"

# Trash not rm
alias rm="trash"

# Better nsxiv
alias nsxiv="nsxiv -aqp"

# Pretty tree
alias tree="lsd --tree"

# Better CP
alias cp="cp -rv"

# Better MKDIR
alias mkdir="mkdir -vp"

# Better MV
alias mv="mv -iv"

# Better CD
alias cd="z"

# Pretty cat
alias cat="bat"

# Better cd ..
alias .="cd .."

# FancyFont
alias ff="fancy_font"

# Better du
alias du="dust"

# Automatic yazi cd
function y() {
    local tmp="$(mktemp -t "yazi-cwd.XXXXXX")" cwd
    yazi "$@" --cwd-file="$tmp"
    if cwd="$(command cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
        builtin cd -- "$cwd"
    fi
    rm -f -- "$tmp"
}

# Pacman
alias pmfi="paru -Sl | awk '{print \$2(\$4==\"\" ? \"\" : \" *\")}' | fzf --multi --preview 'paru -Si {1}' | cut -d \" \" -f 1 | xargs -ro paru -S"
alias pmfr="paru -Q | fzf --multi --preview 'paru -Si {1}' | cut -d \" \" -f 1 | xargs -ro paru -Rns"
alias pmi="paru --bottomup"
alias pmr="paru -Rns"
alias pmu="flatpak update -y && paru --noconfirm -Syyu "
alias pmc="paru -Sc"
alias pmo="paru -Qtdq"
alias pmro="flatpak uninstall --unused && paru -Qtdq | paru -Rns -"

# Quick movement
alias gd="cd ~/Downloads"
alias gp="cd ~/Pictures"
alias gdo="cd ~/Documents"
alias gm="cd /media/Music/Music"
alias gw="cd ~/Pictures/Wallpapers"
alias gc="cd ~/.config"
alias gh="cd ~"
alias gs="cd ~/.local/scripts"
alias gb="cd ~/.local/bin"
alias gB="cd /media/Backups"
alias gr="cd /media"
alias gG="cd /media/Games"
alias gD="cd ~/Documents/Development"
